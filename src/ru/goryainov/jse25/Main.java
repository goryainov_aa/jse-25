package ru.goryainov.jse25;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        List<Object> list = new ArrayList<>();
        list.add(new Person("Ivan", "Ivanov"));
        list.add(new Person("Petr", "Petrov", LocalDate.of(2021, 1, 27)));
        list.add(new Person("Sidor", "Sidorov", LocalDate.of(2021, 1, 27), "dfljbhg@mail.ru"));
        Serializer serializer = new Serializer();
        try {
            serializer.writeToCsvFile(list, " | ", "result.csv");
        } catch (IllegalAccessException | IOException e) {
            logger.log(Level.SEVERE, "Exception: ", e);
        }
    }
}
