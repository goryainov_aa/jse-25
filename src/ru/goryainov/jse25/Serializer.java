package ru.goryainov.jse25;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.util.List;

public class Serializer {

    public void writeToCsvFile(List<Object> objectList, String separator, String fileName) throws IOException, IllegalAccessException {
        checkObjects(objectList);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)))) {
            bufferedWriter.write(getHeading(objectList.get(0).getClass(), separator));
            bufferedWriter.newLine();
            for (Object object : objectList) {
                bufferedWriter.write(getData(object, separator));
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        }
    }

    private static String getHeading(Class<?> clazz, String separator) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Field field : clazz.getDeclaredFields()) {
            stringBuilder.append(field.getName().trim());
            stringBuilder.append(separator);
        }
        return stringBuilder.toString();
    }

    private static String getData(Object object, String separator) throws IllegalAccessException {
        Class<?> clazz = object.getClass();
        StringBuilder stringBuilder = new StringBuilder();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (!(field.get(object) == null || field.get(object) == "")) {
                stringBuilder.append(field.get(object).toString());
                stringBuilder.append(separator);
            }
        }
        return stringBuilder.toString();
    }

    private static void checkObjects(List<Object> objectList) throws IllegalAccessException {
        Class<?> clazz1 = objectList.get(0).getClass();
        for (Object object : objectList) {
            if (clazz1 != object.getClass()) {
                throw new IllegalAccessException();
            }
        }
    }

}
